import json
import urllib.parse
import boto3
from boto3.dynamodb.conditions import Key, Attr
from decimal import Decimal
import time
import datetime

class DecimalEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Decimal):
            return float(obj)
        return json.JSONEncoder.default(self, obj)

def lambda_handler(event, context):
    # HTTP response Object
    responseObject = {}
    responseObject['statusCode'] = 204
    responseObject['message'] = "No data or unknown error, error Handling not fully implemented"
    responseObject['headers'] = {}
    responseObject['headers']['Content-Type'] = "application/json"

    try:
        maxTimeStamp = None
        maxRate = None
        maxRateType = None
        
        pattern = '%Y-%m-%d'
  
        # This query does a table scan to pull all records
        dynamodb = boto3.resource('dynamodb')
        table = dynamodb.Table('rateData')
        response = table.scan()
        data = response['Items']
        for item in data:
            myTimeStr = item['timeStamp']
            epoch = int(time.mktime(time.strptime(myTimeStr, pattern)))
            if (maxTimeStamp is None) or (epoch > maxTimeStamp):
 
                maxTimeStamp = epoch
                maxRateType = item['rateType']
                maxRateValue = item['rateValue']


        time_formatted = time.strftime('%Y-%m-%d', time.localtime(maxTimeStamp))
        print(time_formatted, maxRateType, maxRateValue)
        
        newData = {}
        newData['timeStamp'] = time_formatted
        newData['rateType'] = maxRateType
        newData['rateValue'] = maxRateValue
        
        
        responseObject['message'] = "All Rate records that have been loaded so far"
        responseObject['statusCode'] = 200
        responseObject['body'] = json.dumps(newData)
        return(responseObject)
    except Exception as e:
        responseObject['message'] = "Query Failed"
        responseObject['statusCode'] = 204
        return(responseObject)
