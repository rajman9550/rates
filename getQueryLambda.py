import json
import urllib.parse
import boto3
from boto3.dynamodb.conditions import Key, Attr
from decimal import Decimal


def lambda_handler(event, context):
    queryDate = event['queryStringParameters']['queryDate']
    try:
        dynamodb = boto3.resource('dynamodb')
        table = dynamodb.Table('rateData')

        response = table.get_item(Key={'timeStamp': "2012-02-17"})
        data = response['Item']
        data['message'] = 'Query Completed successfully!'
    except ClientError as e:
        errorMessage = e.response['Error']['Message']
        message = {
            'message': errorMessage
            }

    return {
        'statusCode': 200,
        'headers': {'Content-Type': 'application/json'},
        'body': json.dumps(data)
    }
