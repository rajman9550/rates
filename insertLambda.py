import json
import urllib.parse
import boto3
import csv
import time

print('Loading function')

region='us-east-1'
s3 = boto3.client('s3')
s3_res = boto3.resource('s3')


dyndb = boto3.client('dynamodb', region_name=region)

def lambda_handler(event, context):
    #print("Received event: " + json.dumps(event, indent=2))


    bucket = event['Records'][0]['s3']['bucket']['name']
    key = urllib.parse.unquote_plus(event['Records'][0]['s3']['object']['key'], encoding='utf-8')
    try:
        rateFile= s3.get_object(Bucket=bucket, Key=key)
        lines = rateFile['Body'].read().splitlines(True)


        for item in lines:
            strItem = item.decode("UTF-8")
            result = list()
            result = strItem.split(",")

            rateType = result[0].strip()
            rateValue = result[1].strip()
            timeStamp = result[2].strip()



            #pattern = '%Y-%m-%d'
            #epoch = int(time.mktime(time.strptime(timeStamp, pattern)))
            #poch = str(epoch)

            response = dyndb.put_item(
                TableName='rateData',
                Item={
                    'timeStamp': {'S':timeStamp},
                    'rateValue': {'S':rateValue},
                    'rateType':{'S':rateType}
                })
            print (response)


        newKeyOnlyList = key.split("/")
        newKey = "archive/" + newKeyOnlyList[1]
        oldKey = bucket + "/" + key
        s3_res.Object(bucket,newKey).copy_from(CopySource=oldKey)
        s3_res.Object(bucket,key).delete()
        return rateFile['ContentType']

    except Exception as e:
        print(e)
        print('Error getting object {} from bucket {}. Make sure they exist and your bucket is in the same region as this function.'.format(key, bucket))
        raise e
