
//****************get query gateway endpoint**********************//

resource "aws_api_gateway_rest_api" "getapi" {
  name = "getquery"
}

resource "aws_api_gateway_resource" "getresource" {
  path_part   = "getquery"
  parent_id   = aws_api_gateway_rest_api.getapi.root_resource_id
  rest_api_id = aws_api_gateway_rest_api.getapi.id
}

resource "aws_api_gateway_method" "getmethod" {
  rest_api_id   = aws_api_gateway_rest_api.getapi.id
  resource_id   = aws_api_gateway_resource.getresource.id
  http_method   = "GET"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "get_integration" {
  rest_api_id             = aws_api_gateway_rest_api.getapi.id
  resource_id             = aws_api_gateway_resource.getresource.id
  http_method             = aws_api_gateway_method.getmethod.http_method
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = module.lambdaGetQuery.invoke_lambda_arn
  #module.lambdaScanQuery.invoke_lambda_arn
}

resource "aws_api_gateway_method_response" "get_response_200" {
  rest_api_id = aws_api_gateway_rest_api.getapi.id
  resource_id = aws_api_gateway_resource.getresource.id
  http_method = aws_api_gateway_method.getmethod.http_method
  status_code = "200"
}

resource "aws_api_gateway_integration_response" "GetIntegrationResponse" {
  rest_api_id = aws_api_gateway_rest_api.getapi.id
  resource_id = aws_api_gateway_resource.getresource.id
  http_method = aws_api_gateway_method.getmethod.http_method
  status_code = aws_api_gateway_method_response.get_response_200.status_code

  response_templates = {
    //  "application/json" = "{\"message\":$context.error.messageString}"
    "application/json" = ""
  }
  depends_on = [
    aws_api_gateway_integration.get_integration
  ]

}


# Lambda
resource "aws_lambda_permission" "apigw_get_lambda" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = module.lambdaGetQuery.function_name
  principal     = "apigateway.amazonaws.com"

  # More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
  source_arn = "arn:aws:execute-api:${var.myregion}:${local.accountId}:${aws_api_gateway_rest_api.getapi.id}/*/${aws_api_gateway_method.getmethod.http_method}${aws_api_gateway_resource.getresource.path}"
}

resource "aws_api_gateway_deployment" "getquerydeploy" {
  rest_api_id = aws_api_gateway_rest_api.getapi.id
  triggers = {

    redeployment = sha1(jsonencode([
      aws_api_gateway_resource.getresource.id,
      aws_api_gateway_method.getmethod.id,
      aws_api_gateway_integration.get_integration.id,
    ]))
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_api_gateway_stage" "get_example" {
  deployment_id = aws_api_gateway_deployment.getquerydeploy.id
  rest_api_id   = aws_api_gateway_rest_api.getapi.id
  stage_name    = "test"
}
