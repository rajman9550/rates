resource "aws_dynamodb_table" "rateDataTable" {
  name           = "rateData"
  billing_mode   = "PROVISIONED"
  read_capacity  = 10
  write_capacity = 10
  hash_key       = "timeStamp"


  attribute {
    name = "timeStamp"
    type = "S"
  }


  tags = {
    Name        = "dynamodb-table-rateData"
    Environment = "dev"
  }
}
