output "base_url_max_rate" {
  value = local.url_max
}
output "base_url_scan_query" {
  value = local.url
}
output "base_url_get_query" {
  value = local.url_get
}

output "input_bucket_name" {
  value = aws_s3_bucket.intRateDataBucket.id
}
