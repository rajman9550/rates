import json
import urllib.parse
import boto3
from boto3.dynamodb.conditions import Key, Attr
from decimal import Decimal

class DecimalEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Decimal):
            return float(obj)
        return json.JSONEncoder.default(self, obj)

def lambda_handler(event, context):
    # HTTP response Object
    responseObject = {}
    responseObject['statusCode'] = 204
    responseObject['message'] = "No data or unknown error, error Handling not fully implemented"
    responseObject['headers'] = {}
    responseObject['headers']['Content-Type'] = "application/json"

    try:
        # This query does a table scan to pull all records
        dynamodb = boto3.resource('dynamodb')
        table = dynamodb.Table('rateData')
        response = table.scan()
        data = response['Items']
        responseObject['message'] = "All Rate records that have been loaded so far"
        responseObject['statusCode'] = 200
        responseObject['body'] = data
        return(responseObject)
    except Exception as e:
        return(responseObject)
