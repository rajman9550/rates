output "lambda_arn" {
  value = aws_lambda_function.myLambdaFunction.arn
}
output "lambda_role_arn" {
  value = aws_iam_role.iam_for_lambda.arn
}
output "lambda_role_name" {
  value = aws_iam_role.iam_for_lambda.name
}
output "function_name" {
  value = aws_lambda_function.myLambdaFunction.function_name
}
output "invoke_lambda_arn" {
  value = aws_lambda_function.myLambdaFunction.invoke_arn
}
