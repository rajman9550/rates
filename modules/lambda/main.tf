resource "aws_lambda_function" "myLambdaFunction" {
  filename         = var.python_zip
  function_name    = var.function_name
  role             = aws_iam_role.iam_for_lambda.arn
  handler          = local.handler
  source_code_hash = data.archive_file.lambda_zip.output_base64sha256
  runtime          = "python3.6"
}

resource "aws_iam_role" "iam_for_lambda" {
  name_prefix = "lambda_iam_role-"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}
data "archive_file" "lambda_zip" {
    type          = "zip"
    source_file   = var.python_file
    output_path   = var.python_zip
}

locals {
  handler = join(".",[var.function_name,"lambda_handler"])
}
