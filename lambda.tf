

resource "random_string" "randomBucket" {
  length  = 16
  special = false
  upper   = false
}


# Create Insert Query Lambda
module "lambdaInsertQuery" {
  source        = "./modules/lambda"
  function_name = "insertLambda"
  python_file   = "insertLambda.py"
  python_zip    = "lambda_function.zip"
  handler       = "insertLambda.lambda_handler"
}

# Create Scan Query Lambda
module "lambdaScanQuery" {
  source        = "./modules/lambda"
  function_name = "scanQueryLambda"
  python_file   = "scanQueryLambda.py"
  python_zip    = "lambda_function_scanQuery.zip"
  handler       = "insertLambda.lambda_handler"
}
# Create Get Query Lambda
module "lambdaGetQuery" {
  source        = "./modules/lambda"
  function_name = "getQueryLambda"
  python_file   = "getQueryLambda.py"
  python_zip    = "lambda_function_getQuery.zip"
  handler       = "getQueryLambda.lambda_handler"
}
# Create Max Rate Lambda
module "lambdaMaxRate" {
  source        = "./modules/lambda"
  function_name = "maxRateLambda"
  python_file   = "maxRateLambda.py"
  python_zip    = "lambda_function_maxRate.zip"
  handler       = "maxRateLambda.lambda_handler"
}
# Create all policies for the lambda
resource "aws_iam_policy" "cloudwatch_policy" {
  name   = "cloudwatch_policy"
  policy = templatefile("${path.module}/templates/cloudwatch-policy.tpl", {})
}
# S3 Access Policy
resource "aws_iam_policy" "s3access_policy" {
  name   = "s3access_policy"
  policy = templatefile("${path.module}/templates/s3access-policy.tpl", {})
}

# Dynamodb Read/Write Access Policy
resource "aws_iam_policy" "ddb_policy" {
  name   = "ddb_policy"
  policy = templatefile("${path.module}/templates/dynanodb_access-policy.tpl", {})
}
# Associate policies to lambda
resource "aws_iam_policy_attachment" "lambda_cloudwatch_policy_attachment" {
  name = "lambda_cloudwatch_attachment"
  #roles     = [module.lambdaInsertQuery.lambda_role_name]
  roles      = [module.lambdaInsertQuery.lambda_role_name, module.lambdaScanQuery.lambda_role_name, module.lambdaGetQuery.lambda_role_name, module.lambdaMaxRate.lambda_role_name]
  policy_arn = aws_iam_policy.cloudwatch_policy.arn
}

resource "aws_iam_policy_attachment" "lambda_s3_policy_attachment" {
  name       = "lambda_s3_attachment"
  roles      = [module.lambdaInsertQuery.lambda_role_name, module.lambdaScanQuery.lambda_role_name, module.lambdaGetQuery.lambda_role_name, module.lambdaMaxRate.lambda_role_name]
  policy_arn = aws_iam_policy.s3access_policy.arn
}

resource "aws_iam_policy_attachment" "ddb_policy_attachment" {
  name       = "ddb_policy_attachment"
  roles      = [module.lambdaInsertQuery.lambda_role_name, module.lambdaScanQuery.lambda_role_name, module.lambdaGetQuery.lambda_role_name, module.lambdaMaxRate.lambda_role_name]
  policy_arn = aws_iam_policy.ddb_policy.arn
}

# Create a directory to hold incoming files
resource "aws_s3_bucket" "intRateDataBucket" {
  bucket        = random_string.randomBucket.result
  acl           = var.bucket_acl
  force_destroy = true
  versioning {
    enabled = var.bucket_version
  }
}


resource "aws_s3_bucket_object" "incoming" {
  bucket = aws_s3_bucket.intRateDataBucket.id
  acl    = var.bucket_acl
  key    = "incoming/"
}

# Create a directory to archive  incoming files

resource "aws_s3_bucket_object" "archive" {
  bucket = aws_s3_bucket.intRateDataBucket.id
  acl    = var.bucket_acl
  key    = "archive/"
}


# Set up permissions
resource "aws_lambda_permission" "insertLambda_permissions" {
  statement_id = "AllowExecutionFromS3"
  action       = "lambda:InvokeFunction"

  function_name = module.lambdaInsertQuery.function_name
  principal     = "s3.amazonaws.com"
  source_arn    = aws_s3_bucket.intRateDataBucket.arn

}
# Set up permissions
resource "aws_lambda_permission" "queryLambda_permissions" {
  statement_id = "AllowExecutionFromS3"
  action       = "lambda:InvokeFunction"

  function_name = module.lambdaScanQuery.function_name
  principal     = "s3.amazonaws.com"
  source_arn    = aws_s3_bucket.intRateDataBucket.arn

}


resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = aws_s3_bucket.intRateDataBucket.id

  lambda_function {
    lambda_function_arn = module.lambdaInsertQuery.lambda_arn
    events              = ["s3:ObjectCreated:*"]
    filter_prefix       = "incoming/"
    filter_suffix       = ".csv"
  }
  depends_on = [aws_lambda_permission.insertLambda_permissions]
}
