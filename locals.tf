locals {
  path_max = "dev/maxrate"
  url_max  = "${aws_api_gateway_deployment.maxquerydeploy.invoke_url}${local.path_max}"
  path = "example/scanquery"
  url  = "${aws_api_gateway_deployment.scanquerydeploy.invoke_url}${local.path}"
  path_get = "test/getquery?queryDate="
  url_get  = "${aws_api_gateway_deployment.getquerydeploy.invoke_url}${local.path_get}"
  accountId = data.aws_caller_identity.current.account_id
}
