terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~>3.0"
    }
  }
}

# Switch to using local backend as this is a single user env
/*
terraform {
  backend "s3" {
    bucket = "terraform-state-raj-04122021-1622"
    key    = "terraform/state/key"
    region = "us-east-1"
  }
}
*/

provider "aws" {
  region = var.regionName
}
