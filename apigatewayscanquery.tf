
//****************scanquery gateway endpoint**********************//

resource "aws_api_gateway_rest_api" "scanapi" {
  name = "scanquery"
}

resource "aws_api_gateway_resource" "resource1" {
  path_part   = "scanquery"
  parent_id   = aws_api_gateway_rest_api.scanapi.root_resource_id
  rest_api_id = aws_api_gateway_rest_api.scanapi.id
}

resource "aws_api_gateway_method" "method" {
  rest_api_id   = aws_api_gateway_rest_api.scanapi.id
  resource_id   = aws_api_gateway_resource.resource1.id
  http_method   = "GET"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "scan_integration" {
  rest_api_id             = aws_api_gateway_rest_api.scanapi.id
  resource_id             = aws_api_gateway_resource.resource1.id
  http_method             = aws_api_gateway_method.method.http_method
  integration_http_method = "POST"
  type                    = "AWS"
  uri                     = module.lambdaScanQuery.invoke_lambda_arn
  #module.lambdaScanQuery.invoke_lambda_arn
}

resource "aws_api_gateway_method_response" "response_200" {
  rest_api_id = aws_api_gateway_rest_api.scanapi.id
  resource_id = aws_api_gateway_resource.resource1.id
  http_method = aws_api_gateway_method.method.http_method
  status_code = "200"
}

resource "aws_api_gateway_integration_response" "ScanIntegrationResponse" {
  rest_api_id = aws_api_gateway_rest_api.scanapi.id
  resource_id = aws_api_gateway_resource.resource1.id
  http_method = aws_api_gateway_method.method.http_method
  status_code = aws_api_gateway_method_response.response_200.status_code

  response_templates = {
    //  "application/json" = "{\"message\":$context.error.messageString}"
    "application/json" = ""
  }
  depends_on = [
    aws_api_gateway_integration.scan_integration
  ]

}


# Lambda
resource "aws_lambda_permission" "apigw_lambda" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = module.lambdaScanQuery.function_name
  principal     = "apigateway.amazonaws.com"

  # More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
  source_arn = "arn:aws:execute-api:${var.myregion}:${local.accountId}:${aws_api_gateway_rest_api.scanapi.id}/*/${aws_api_gateway_method.method.http_method}${aws_api_gateway_resource.resource1.path}"
}

resource "aws_api_gateway_deployment" "scanquerydeploy" {
  rest_api_id = aws_api_gateway_rest_api.scanapi.id
  triggers = {

    redeployment = sha1(jsonencode([
      aws_api_gateway_resource.resource1.id,
      aws_api_gateway_method.method.id,
      aws_api_gateway_integration.scan_integration.id,
    ]))
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_api_gateway_stage" "example" {
  deployment_id = aws_api_gateway_deployment.scanquerydeploy.id
  rest_api_id   = aws_api_gateway_rest_api.scanapi.id
  stage_name    = "example"
}
