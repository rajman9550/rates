
//****************max rate gateway endpoint**********************//

resource "aws_api_gateway_rest_api" "maxapi" {
  name = "maxrate"
}

resource "aws_api_gateway_resource" "maxresource" {
  path_part   = "maxrate"
  parent_id   = aws_api_gateway_rest_api.maxapi.root_resource_id
  rest_api_id = aws_api_gateway_rest_api.maxapi.id
}

resource "aws_api_gateway_method" "maxmethod" {
  rest_api_id   = aws_api_gateway_rest_api.maxapi.id
  resource_id   = aws_api_gateway_resource.maxresource.id
  http_method   = "GET"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "max_integration" {
  rest_api_id             = aws_api_gateway_rest_api.maxapi.id
  resource_id             = aws_api_gateway_resource.maxresource.id
  http_method             = aws_api_gateway_method.maxmethod.http_method
  integration_http_method = "POST"
  type                    = "AWS"
  uri                     = module.lambdaMaxRate.invoke_lambda_arn
  #module.lambdaScanQuery.invoke_lambda_arn
}

resource "aws_api_gateway_method_response" "max_response_200" {
  rest_api_id = aws_api_gateway_rest_api.maxapi.id
  resource_id = aws_api_gateway_resource.maxresource.id
  http_method = aws_api_gateway_method.maxmethod.http_method
  status_code = "200"
}

resource "aws_api_gateway_integration_response" "MaxIntegrationResponse" {
  rest_api_id = aws_api_gateway_rest_api.maxapi.id
  resource_id = aws_api_gateway_resource.maxresource.id
  http_method = aws_api_gateway_method.maxmethod.http_method
  status_code = aws_api_gateway_method_response.max_response_200.status_code

  response_templates = {
    //  "application/json" = "{\"message\":$context.error.messageString}"
    "application/json" = ""
  }
  depends_on = [
    aws_api_gateway_integration.max_integration
  ]

}


# Lambda
resource "aws_lambda_permission" "max_apigw_lambda" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = module.lambdaMaxRate.function_name
  principal     = "apigateway.amazonaws.com"

  # More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
  source_arn = "arn:aws:execute-api:${var.myregion}:${local.accountId}:${aws_api_gateway_rest_api.maxapi.id}/*/${aws_api_gateway_method.maxmethod.http_method}${aws_api_gateway_resource.maxresource.path}"
}

resource "aws_api_gateway_deployment" "maxquerydeploy" {
  rest_api_id = aws_api_gateway_rest_api.maxapi.id
  triggers = {

    redeployment = sha1(jsonencode([
      aws_api_gateway_resource.maxresource.id,
      aws_api_gateway_method.maxmethod.id,
      aws_api_gateway_integration.max_integration.id,
    ]))
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_api_gateway_stage" "max_example" {
  deployment_id = aws_api_gateway_deployment.maxquerydeploy.id
  rest_api_id   = aws_api_gateway_rest_api.maxapi.id
  stage_name    = "dev"
}
