## Table of Contents
1. [General Info](#general-info)
2. [Requirements](#technologies)
3. [Assumptions](#assumptions)
4. [Installation](#installation)
5. [Installation Output - Must  record](#outputs)
6. [Test](#test)
7. [Destroy](#wrapup)
8. [Assessment](#assessment)


### General Info
***
This project is to create an interest rate retrieval application that will process incoming interest rate data into a data store and be retrievable via an API call. All AWS resources will be provisioned via Terraform. Users will drop a rate file in the incoming folder of a S3 bucket, once processed it will be moved to archive folder in the same bucket. All back end code written in Python.

### Architecture Diagram
[Architecture Diagram](https://gitlab.com/rajman9550/rates/-/blob/master/architecture.JPG)

## Technology Requirement
***
* Tests Performed
  - AWS account/user
    - Programmatic access
    - Create access key ID and secret access key
  - AWS Region
    - Tested in us-east-1
  - Hashicorp AWS Terraform Provider Version >= 3.0
  - Your local computer must have
    - git
    - Terraform version >= 0.13
      - Tested with 0.14.7

## Assumptions
***
* Data to be uploaded will be  CSV format and have  follow the pattern
  - interestRateType, interestRateValue, timeStamp
    - timeStamp format : YYYY-MM-DD
    - No other file extensions other then csv will be processed
* Rate is assumed to change only once per day and hence one rate data per timeStamp
* A local back end for Terraform state is used as this is a test project

## Installation
***
* git clone https://gitlab.com/rajman9550/rates.git
* cd rates
  - Apply the following steps
* create a file called "terraform.tfvars" and include te following in that file
  - aws_access_key = "YOUR_ACCESS_KEY"
  - aws_secret_key = "YOUR_SECRET_KEY"
* terraform init
* terraform plan
* terraform apply

## Terraform Output - Must  record
* Write down the following outputs from Terraform apply
  - input_bucket_name : Name of the bucket where you will drop the rate file
  - base_url_get_query : API request to get rate at a given point in time
  - base_url_max_rate : API request to return the latest/most current interest rate based on time Stamp
  - base_url_scan_query : API request to return the list of all interest rates

## Test
***
  * The rates folder contains a sample load file called rateSheet.CSV
    - You can update to include or remove rows
    - Use your own rate document
      - Must be in CSV format
* Up load the sample rate file called rateSheet.csv into the bucket that was identfied above in the Installation section to a folder called incoming
  - AWS CLI upload Example:
    - aws s3 cp ratesheet.csv  s3://asasasas/incoming/ratesheet.csv
  - You can also use AWS console and upload
* Access the API end points that you have recorded from the  Terraform Output section to query rates
* **Test Issues**
  - Upon multiple tear down and rebuilds, noticed that API gateway occasinally throws a 500 Internal Server erroor for API queries
    - If this happens, please wait for a few minutes and retry

## Destroy & Cleanup

* cd rates
* terraform destroy

## Assessment

* Python
  - Lint not done
  - JSON output format not clean and standardized across APIs
* Database
  - Ran into challenges with one of the APIs as Dynanodb does not perform aggregations
  - Indexing not implemented
* terraform
  - Currently not using remote back backend for state
  - Need to have better, consistent naming conventions
