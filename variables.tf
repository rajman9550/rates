variable "regionName" {
  type        = string
  description = "Name of Region for Bucket"
  default     = "us-east-1"
}

variable "bucket_version" {
  type        = bool
  description = "Set to true, if versioning is needed or set to False"
  default     = true
}

variable "bucket_acl" {
  type        = string
  description = "ACL Status"
  default     = "public-read-write"
}

variable "myregion" {
  default = "us-east-1"
}
